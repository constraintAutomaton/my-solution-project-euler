# Multiples of 3 or 5

https://projecteuler.net/problem=1
# Problem

<p>If we list all the natural numbers below $10$ that are multiples of $3$ or $5$, we get $3, 5, 6$ and $9$. The sum of these multiples is $23$.</p>
<p>Find the sum of all the multiples of $3$ or $5$ below $1000$.</p>


# Solution
We want $\sum_{i=3}^{n}a_i$ where $( a_i \pmod{3} =0 \lor a_i \pmod{5} = 0) \land a_i<1000$

We can have a naive implementation or make a sum from a serie that jump by 3 another that jump by 5 and substract by one that jump by 15 (because they are in common).
