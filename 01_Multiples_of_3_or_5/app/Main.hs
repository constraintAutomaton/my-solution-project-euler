module Main where

import HaskellSay (haskellSay)
import System.TimeIt

main :: IO ()
main = do 
    haskellSay "Multiples of 3 or 5"
    timeIt $ putStrLn ("result: " ++ show total)
    timeIt $ putStrLn ("result alternatif: " ++ show totalAlternatif)

serie :: Int->[Int]
serie limit = [x|x<- [3..limit-1], x `mod` 3 == 0 || x `mod` 5 == 0 ]

n :: Int
n = 1000 :: Int

total :: Int
total = sum (serie n)

totalAlternatif :: Int
totalAlternatif = sum [3, 6 ..n] + sum [5, 10 ..n] - sum [15, 30 ..n] 


    
